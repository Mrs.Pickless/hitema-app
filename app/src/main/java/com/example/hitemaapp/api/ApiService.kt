package com.example.hitemaapp.api

import com.example.hitemaapp.models.User
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("/users")
    fun fetchAllUsers(): Call<List<User>>
}